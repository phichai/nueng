package com.RetailSoft.shop_op;

import android.app.Activity;
import android.app.DownloadManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.PieChartView;


/**
 * Created by IS-PATTHANAPONG on 04/04/2559.
 */
public class BDReport2 extends Activity {

    private final String[] mLabels= {"Target","BD Archive"};
    private final float [][] mValues = {{26.5f,15f}, {7.5f,3f}};
    private BDReport2 mContext;
    private PieChartView chart;
    private PieChartView callchart;
    public PieChartData data;
    public PieChartData calldata;
    private boolean hasLabels = true;
    private boolean hasLabelsOutside = false;
    private boolean hasCenterCircle = true;
    private boolean hasCenterText1 = true;
    private boolean hasCenterText2 = true;
    private boolean isExploded = false;
    private boolean hasLabelForSelected = false;
    private getCallData mGetCallData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bdchart2);
        chart = (PieChartView) findViewById(R.id.chart);
        callchart = (PieChartView) findViewById(R.id.callchart);
        mContext = this;

        mGetCallData = new getCallData();
        mGetCallData.execute();
        //generateData();
    }

    private void generateData() {

        if(hasCenterCircle){
            //data.setCenterCircleColor(R.color.lightgray2);
            chart.setBackgroundColor(mContext.getResources().getColor(R.color.lightgraysolid));
            callchart.setBackgroundColor(mContext.getResources().getColor(R.color.lightgraysolid));
        }
        chart.setPieChartData(data);
        callchart.setPieChartData(calldata);

    }

    class getCallData extends AsyncTask<String, Integer, Void> {

        @Override
        protected Void doInBackground(String... params) {
            String url = "http://mshop.ssup.co.th/shop_op/bdaymem_report.php";
            OkHttpClient okHttpClient = new OkHttpClient();
            Request request = new Request.Builder()
                        .url(url)
                        .build();
                        /**/
            Response responses = null;
            List<SliceValue> values = new ArrayList<SliceValue>();
            data = new PieChartData(values);
            try {
                responses = okHttpClient.newCall(request).execute();
                String jsonData = responses.body().string();
                Log.i("JSON Data", jsonData);
                JSONArray jObject = new JSONArray(jsonData);
                JSONObject subObject = jObject.getJSONObject(0);
                int target = subObject.getInt("target");
                int toTarget = subObject.getInt("totarget");
                int gross = subObject.getInt("gross");
                int calltarget = subObject.getInt("calltarget");
                int remain = subObject.getInt("remain");
                int callcount = subObject.getInt("callcount");

                SliceValue sliceValue = new SliceValue(toTarget, mContext.getResources().getColor(R.color.g1solid));
                sliceValue.setLabel("To Target :"+String.valueOf(toTarget));
                values.add(sliceValue);
                sliceValue = new SliceValue(gross, mContext.getResources().getColor(R.color.g2solid));
                sliceValue.setLabel("Gross :" + String.valueOf(gross));
                values.add(sliceValue);

                data = new PieChartData(values);
                data.setHasLabels(hasLabels);
                data.setHasLabelsOnlyForSelected(hasLabelForSelected);
                data.setHasLabelsOutside(hasLabelsOutside);
                data.setHasCenterCircle(hasCenterCircle);

                values = new ArrayList<SliceValue>();
                sliceValue = new SliceValue(remain, mContext.getResources().getColor(R.color.g1solid));
                sliceValue.setLabel("Call Remain :" + String.valueOf(remain));
                values.add(sliceValue);
                sliceValue = new SliceValue(callcount, mContext.getResources().getColor(R.color.g2solid));
                sliceValue.setLabel("Called :" + String.valueOf(callcount));
                values.add(sliceValue);

                calldata = new PieChartData(values);
                calldata.setHasLabels(hasLabels);
                calldata.setHasLabelsOnlyForSelected(hasLabelForSelected);
                calldata.setHasLabelsOutside(hasLabelsOutside);
                calldata.setHasCenterCircle(hasCenterCircle);
                if (hasCenterText1) {
                    data.setCenterText1("Target");
                    Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Italic.ttf");
                    data.setCenterText1Typeface(tf);
                    data.setCenterText1FontSize(ChartUtils.px2sp(getResources().getDisplayMetrics().scaledDensity,
                            (int) getResources().getDimension(R.dimen.pie_chart_text1_size)));
                    calldata.setCenterText1("Calling");
                    calldata.setCenterText1Typeface(tf);
                    calldata.setCenterText1FontSize(ChartUtils.px2sp(getResources().getDisplayMetrics().scaledDensity,
                            (int) getResources().getDimension(R.dimen.pie_chart_text1_size)));
                }
                if (hasCenterText2) {
                    data.setCenterText2("Target : "+String.valueOf(target));

                    Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "Roboto-Italic.ttf");

                    data.setCenterText2Typeface(tf);
                    data.setCenterText2FontSize(ChartUtils.px2sp(getResources().getDisplayMetrics().scaledDensity,
                            (int) getResources().getDimension(R.dimen.pie_chart_text2_size)));

                    calldata.setCenterText2("Call Target : "+String.valueOf(calltarget));
                    calldata.setCenterText2Typeface(tf);
                    calldata.setCenterText2FontSize(ChartUtils.px2sp(getResources().getDisplayMetrics().scaledDensity,
                            (int) getResources().getDimension(R.dimen.pie_chart_text2_size)));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            generateData();
        }
    }
}
