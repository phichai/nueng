package com.RetailSoft.shop_op;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.RetailSoft.shop_op.util.BitmapUtil;
import com.RetailSoft.shop_op.util.JSONVideoFeeder;
import com.RetailSoft.shop_op.util.OnJSONReceiver;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class SelfSurvice extends ListActivity implements OnJSONReceiver {

	protected static final int TAKE_PICTURE = 1;
	private static SelfSurvice mContext;
	private SharedPreferences prefs;
	private String userID;
	private String shopName;
	private Button mYearBtn;
	private String defBg;
	private Bitmap bm;
	private BitmapUtil mBmUtil;
	private ImageView mVVbg;
	private TextView mAppName;
	private Button mTypeBtn;
	private int year;
	private Button mPrivBtn;
	private ArrayList<String> subOptionFileArr = new ArrayList<>(10);
	private String subOption;
	private Calendar myCalendar;
	private EditText mStartdate;
	private EditText mStopdate;
	private EditText updateEditText;

	private static List<HashMap<String, String>> mDataList;
	private EfficientAdapter adapter;

	private ImageView mCmbtn;
	private Uri mCapturedImageURI;
	private String encodedImg;
	private String encodedImg2;
	private String encodedImg3;
	private PostDataTask mPosData;
	private TextView mRemain;
	private EditText mCost;
	private String wform;
	private String wfor;
	private EditText mHostName;
	private EditText mBecause;
	private EditText mRemark;
	private Button mSubmitBtn;
	private ImageView mCmbtn2;
	private ImageView mCmbtn3;
	private String fileName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.selfsurvice);

		mContext = this;
		bindwidget();
		setListener();
		myCalendar = Calendar.getInstance();


		prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		defBg = prefs.getString("defWall", "");
		userID = prefs.getString("userLogin", "");
		shopName = prefs.getString("shopid","");
		mBmUtil = new BitmapUtil();
		Calendar now = Calendar.getInstance();
		year = now.get(Calendar.YEAR)+543;

		if (defBg.equalsIgnoreCase("")) {

		} else {
			bm = mBmUtil.StringToBitMap(defBg);
			mVVbg.setImageBitmap(bm);
		}

		mPosData = new PostDataTask();
		mPosData.execute();
	}

	private void bindwidget() {
		// TODO Auto-generated method stub
		mYearBtn = (Button) findViewById(R.id.yearBtn);
		mTypeBtn = (Button) findViewById(R.id.typeBtn);
		mVVbg = (ImageView) findViewById(R.id.vvbg);
		mAppName = (TextView) findViewById(R.id.appnameshow);
		mPrivBtn = (Button) findViewById(R.id.privBtn);
		mStartdate = (EditText) findViewById(R.id.start_date);
		mStopdate = (EditText) findViewById(R.id.stop_date);

		mCmbtn = (ImageView) findViewById(R.id.cmBtn);
		mCmbtn2 = (ImageView) findViewById(R.id.cmBtn2);
		mCmbtn3  =(ImageView) findViewById(R.id.cmBtn3);
		/*form data*/
		mRemain = (TextView) findViewById(R.id.remain);
		mCost = (EditText) findViewById(R.id.cost);
		mHostName = (EditText) findViewById(R.id.hostpital_name);
		mBecause = (EditText) findViewById(R.id.desc1_name);
		mRemark = (EditText) findViewById(R.id.desc2_name);
		mSubmitBtn = (Button) findViewById(R.id.submitBtn);
	}

	private void setListener() {
		// TODO Auto-generated method stub
		
		mSubmitBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SaveDataTask mSaveData = new SaveDataTask();
				mSaveData.execute();
			}
		});
		
		mCmbtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				Intent cameraIntent = new
				Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(cameraIntent, TAKE_PICTURE);
				/**/
				fileName = "temp.jpg";
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.TITLE, fileName);
				mCapturedImageURI = getContentResolver().insert(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
				startActivityForResult(intent, TAKE_PICTURE);
			}
		});

		mCmbtn2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				fileName = "temp2.jpg";
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.TITLE, fileName);
				mCapturedImageURI = getContentResolver().insert(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
				startActivityForResult(intent, TAKE_PICTURE);
			}
		});

		mCmbtn3.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				fileName = "temp3.jpg";
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.TITLE, fileName);
				mCapturedImageURI = getContentResolver().insert(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
				startActivityForResult(intent, TAKE_PICTURE);
			}
		});
		mYearBtn.setOnClickListener(new View.OnClickListener() {
		public String outtext;

		@Override
		public void onClick(View v) {
			final Dialog dialog = new Dialog(mContext);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.std_dialog);

			TextView mTitle = (TextView) dialog.findViewById(R.id.tv1);
			mTitle.setText("เลือกปี");
			final RadioGroup mRGroup = (RadioGroup) dialog.findViewById(R.id.rgroup);
			Button saveBtn = (Button) dialog.findViewById(R.id.saveBtn);

			for (int i=0;i<2;i++){
				RadioButton mbtn = new RadioButton(mContext);
				mbtn.setText(String.valueOf(year+i));
				mRGroup.addView(mbtn);
			}

			mRGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					RadioButton checkedRadioButton = (RadioButton) dialog.findViewById(checkedId);
					outtext = checkedRadioButton.getText().toString();

				}
			});
			saveBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mYearBtn.setText(outtext);
					dialog.dismiss();

				}
			});
			dialog.show();
		}
	});
		mTypeBtn.setOnClickListener(new View.OnClickListener() {
			public String outtext;

			@Override
			public void onClick(View v) {
				final Dialog dialog = new Dialog(mContext);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.std_dialog);

				TextView mTitle = (TextView) dialog.findViewById(R.id.tv1);
				mTitle.setText("เลือกประเภท");
				final RadioGroup mRGroup = (RadioGroup) dialog.findViewById(R.id.rgroup);
				Button saveBtn = (Button) dialog.findViewById(R.id.saveBtn);
				String file = "appmenu.txt";
				String menuData = readMenu(file);
				try {
					JSONArray jObject = new JSONArray(menuData);
					for (int i = 0; i < jObject.length(); i++) {
						JSONObject subObject = jObject.getJSONObject(i);
						String rname = subObject.getString("rname");
						String rvalue = subObject.getString("rvalue");
						String rfile = subObject.getString("rfile");
						subOptionFileArr.add(rfile);
						RadioButton mbtn = new RadioButton(mContext);
						mbtn.setContentDescription(rvalue);
						mbtn.setText(rname);
						mRGroup.addView(mbtn);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				mRGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {


					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {

						RadioButton checkedRadioButton = (RadioButton) dialog.findViewById(checkedId);
						String typeVar = checkedRadioButton.getContentDescription().toString();
						wform = typeVar;
						if(typeVar.equalsIgnoreCase("2")){
							mPrivBtn.setText("สามี/ภรรยา");

						}else if(typeVar.equalsIgnoreCase("3")){
							mPrivBtn.setText("บุตร");

						}else if(typeVar.equalsIgnoreCase("4")){
							mPrivBtn.setText("บุตร");

						}else{
							mPrivBtn.setText("เลือก");
						}
						Log.i("PTR DATA",typeVar);
						int ptr = Integer.parseInt(typeVar)-1;

						//subOption = subOptionFileArr.get();
						int loop = subOptionFileArr.size();
						for(int i=0;i<loop;i++){
							if(i==ptr){
								Log.i("PTR DATA",String.valueOf(i)+":"+String.valueOf(ptr)+":"+subOptionFileArr.get(i).toString());
								subOption = subOptionFileArr.get(i).toString();
							}
						}
						outtext = checkedRadioButton.getText().toString();

					}
				});
				saveBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						mTypeBtn.setText(outtext);
						dialog.dismiss();
					}
				});
				dialog.show();
			}
		});
		mPrivBtn.setOnClickListener(new View.OnClickListener() {
			public String outtext;
			@Override
			public void onClick(View v) {
				Log.i("PTR DATA", subOption);
				if(subOption.startsWith("suboption")){
					final Dialog dialog = new Dialog(mContext);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.std_dialog);
					String menuData = readMenu(subOption);

					TextView mTitle = (TextView) dialog.findViewById(R.id.tv1);
					mTitle.setText("สิทธิ์สำหรับ");
					final RadioGroup mRGroup = (RadioGroup) dialog.findViewById(R.id.rgroup);
					Button saveBtn = (Button) dialog.findViewById(R.id.saveBtn);
					try {
						JSONArray jObject = new JSONArray(menuData);
						for (int i = 0; i < jObject.length(); i++) {
							JSONObject subObject = jObject.getJSONObject(i);
							String rname = subObject.getString("rname");
							String rvalue = subObject.getString("rvalue");
							RadioButton mbtn = new RadioButton(mContext);
							mbtn.setContentDescription(rvalue);
							mbtn.setText(rname);
							mRGroup.addView(mbtn);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					mRGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {


						@Override
						public void onCheckedChanged(RadioGroup group, int checkedId) {
							RadioButton checkedRadioButton = (RadioButton) dialog.findViewById(checkedId);
							String typeVar = checkedRadioButton.getContentDescription().toString();

							wfor = typeVar;
							Log.i("Post data",wfor);
							outtext = checkedRadioButton.getText().toString();
						}
					});
					saveBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mPrivBtn.setText(outtext);
							dialog.dismiss();
						}
					});
					dialog.show();
				}else{

				}

			}

		});
		mStartdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				updateEditText = mStartdate;
				new DatePickerDialog(mContext, date, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();
					
			}
		});
		mStopdate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				updateEditText = mStopdate;
				new DatePickerDialog(mContext, date, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		});
	}

	public String readMenu(String fname) {
		InputStream input;
		String text="";
		try {
			AssetManager manager = mContext.getAssets();
			input = manager.open(fname);
			int size = input.available();
			byte[] buffer = new byte[size];
			input.read(buffer);
			input.close();
			text = new String(buffer);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			input = null;
		}
		return text;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case TAKE_PICTURE:
				if (resultCode == Activity.RESULT_OK) {

				/**/
					String[] projection = { MediaStore.Images.Media.DATA };
					Cursor cursor = managedQuery(mCapturedImageURI, projection,
							null, null, null);
					int column_index_data = cursor
							.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					String capturedImageFilePath = cursor
							.getString(column_index_data);

					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					Matrix matrix = new Matrix();
					matrix.postRotate(90);


					Bitmap thumbnail = getThumbnailBitmap(capturedImageFilePath,
							568);
					thumbnail = Bitmap.createBitmap(thumbnail,0,0,thumbnail.getWidth(),thumbnail.getHeight(),matrix,true);
					thumbnail.compress(Bitmap.CompressFormat.JPEG, 60, baos);
					byte[] b = baos.toByteArray();
					Log.i("",fileName);
					if(fileName.equalsIgnoreCase("temp.jpg")){
						mCmbtn.setImageBitmap(thumbnail);
						encodedImg = Base64.encodeToString(b, Base64.DEFAULT);
					}else if (fileName.equalsIgnoreCase("temp2.jpg")){
						mCmbtn2.setImageBitmap(thumbnail);
						encodedImg2 = Base64.encodeToString(b, Base64.DEFAULT);
					}else if(fileName.equalsIgnoreCase("temp3.jpg")){
						mCmbtn3.setImageBitmap(thumbnail);
						encodedImg3 = Base64.encodeToString(b, Base64.DEFAULT);
					}




				/**/
					// Log.i("path", capturedImageFilePath);
					thumbnail = null;
				}
		}
	}

	/*
	 * @Override public void onActivityResult(int requestCode, int resultCode,
	 * Intent intent) { //Log.i("result",String.valueOf(resultCode)); super
	 * Log.i("result Intetnt",String.valueOf(intent.getScheme()));
	 * 
	 * finish(); }
	 * 
	 * /*
	 */
	public Bitmap getThumbnailBitmap(String path, int thumbnailSize) {
		BitmapFactory.Options bounds = new BitmapFactory.Options();
		bounds.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, bounds);
		if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
			return null;
		}
		int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
				: bounds.outWidth;
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = originalSize / thumbnailSize;
		return BitmapFactory.decodeFile(path, opts);
	}

	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
							  int dayOfMonth) {
			// TODO Auto-generated method stub
			myCalendar.set(Calendar.YEAR, year);
			myCalendar.set(Calendar.MONTH, monthOfYear);
			myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			updateLabel();
		}



	};

	private void updateLabel() {
		String myFormat = "dd/MM/yyyy"; //In which you need put here
		SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.ENGLISH);

		updateEditText.setText(sdf.format(myCalendar.getTime()));
	}

	public void changDocset(){

	}

	@Override
	public void onReceiveData(ArrayList<HashMap<String, String>> result) {
		mDataList = result;
		setListAdapter(adapter);

	}



	private static class EfficientAdapter extends BaseAdapter {

		private LayoutInflater mInflater;


		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			int _row = (int) mDataList.size();
			return _row;

		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;

			final int pos = position;

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.stdrow, null);
				holder = new ViewHolder();
				holder.clayout = (LinearLayout) convertView
						.findViewById(R.id.clayout);
				holder.iButton = (Button) convertView.findViewById(R.id.appBtn);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final String filename = mDataList.get(pos).get(JSONVideoFeeder.KEY_FILE);
			final String urlfile = mDataList.get(pos).get(JSONVideoFeeder.KEY_URL);
			Log.i("URL DATA",urlfile+filename);
			if(mDataList.get(pos).get(JSONVideoFeeder.KEY_FSTATUS).equalsIgnoreCase("yes")){
				holder.iButton.setText(mDataList.get(pos).get(JSONVideoFeeder.KEY_TITLE));
				holder.iButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent mIntent = null;
						mIntent = new Intent(mContext, PlayVideo.class);
						mIntent.putExtra("filename", urlfile+filename);
						mContext.startActivity(mIntent);
					}
				});
			}else{
				holder.iButton.setText("Download : "+mDataList.get(pos).get(JSONVideoFeeder.KEY_TITLE));
				holder.iButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent mIntent = null;
						mIntent = new Intent(mContext, DownloadVideo.class);
						String newfile = urlfile + filename;
						mIntent.putExtra("filename", filename);
						Log.i("URL DATASEND",newfile);
						mIntent.putExtra("urlpost", urlfile.replace("mcs/learn/", "mcs/learn/download_media.php"));
						mContext.startActivity(mIntent);
					}
				});
			}





			/**/
			return convertView;

		}

		static class ViewHolder {
			Button iButton;
			TextView iText;
			LinearLayout clayout;
		}

	}
	class PostDataTask extends AsyncTask<String, Integer, Void> {

		public StringBuilder str;
		private DefaultHttpClient client;
		private HttpPost httpPost;
		private ProgressDialog loadingDialog;
		private String chklistTxt;
		private String chklistDetail;

		@Override
		protected Void doInBackground(String... params) {
			List<NameValuePair> prams = new ArrayList<NameValuePair>();

			str = new StringBuilder();
			String url = "http://mshop.ssup.co.th/shop_op/welfare.php";
			client = new DefaultHttpClient();
			httpPost = new HttpPost(url);

			try {

				prams.add(new BasicNameValuePair("user",userID));
				prams.add(new BasicNameValuePair("brand", shopName));


				httpPost.setEntity(new UrlEncodedFormEntity(prams, HTTP.UTF_8));

				HttpResponse response = client.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();

				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) { // Status OK
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						str.append(line);
					}
				} else {
					Log.e("Log", "Failed to connect server..");
				}

			} catch (ClientProtocolException e) {
				Log.i("ERR1", "Post fail");
				e.printStackTrace();
			} catch (IOException e) {
				Log.i("ERR2", "Data fail");
				e.printStackTrace();
			}finally{
				prams = null;
				url = null;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			Log.i("Post response",str.toString());
			JSONObject subObject = null;
			String balance="";
			try {
				subObject = new JSONObject(str.toString());
				balance = subObject.getString("balance");
				String confirm = subObject.getString("amt_confirm");
				mRemain.setText(balance);
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}

	class SaveDataTask extends AsyncTask<String, Integer, Void> {

		public StringBuilder str;
		private DefaultHttpClient client;
		private HttpPost httpPost;
		private ProgressDialog loadingDialog;
		private String chklistTxt;
		private String chklistDetail;
		private String amont;
		private String hostname;
		private String startd;
		private String stopd;
		private String bcdesc;
		private String remarkDesc;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			amont = mCost.getText().toString();
			hostname = mHostName.getText().toString();
			startd = mStartdate.getText().toString();
			stopd = mStopdate.getText().toString();
			bcdesc = mBecause.getText().toString();
			remarkDesc = mRemark.getText().toString();
		}

		@Override
		protected Void doInBackground(String... params) {
			List<NameValuePair> prams = new ArrayList<NameValuePair>();

			str = new StringBuilder();
			String url = "http://mshop.ssup.co.th/shop_op/welfare.php";
			client = new DefaultHttpClient();
			httpPost = new HttpPost(url);

			try {
				prams.add(new BasicNameValuePair("user",userID));
				prams.add(new BasicNameValuePair("brand", shopName));
				prams.add(new BasicNameValuePair("formtype", "add"));
				prams.add(new BasicNameValuePair("amount", amont));
				prams.add(new BasicNameValuePair("welfare_form", wform));
				prams.add(new BasicNameValuePair("welfare_for", wfor));
				prams.add(new BasicNameValuePair("welfare_where", hostname));
				prams.add(new BasicNameValuePair("usage_start", startd));
				prams.add(new BasicNameValuePair("usage_stop", stopd));
				prams.add(new BasicNameValuePair("welfare_because", bcdesc));
				prams.add(new BasicNameValuePair("welfare_remark", remarkDesc));
				prams.add(new BasicNameValuePair("imageupload", encodedImg));
				prams.add(new BasicNameValuePair("imageupload2", encodedImg2));
				prams.add(new BasicNameValuePair("imageupload3", encodedImg3));
				httpPost.setEntity(new UrlEncodedFormEntity(prams, HTTP.UTF_8));

				HttpResponse response = client.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();

				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) { // Status OK
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						str.append(line);
					}
				} else {
					Log.e("Log", "Failed to connect server..");
				}

			} catch (ClientProtocolException e) {
				Log.i("ERR1", "Post fail");
				e.printStackTrace();
			} catch (IOException e) {
				Log.i("ERR2", "Data fail");
				e.printStackTrace();
			}finally{
				prams = null;
				url = null;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			Log.i("Post response",str.toString());
			JSONObject subObject = null;
			try {
				subObject = new JSONObject(str.toString());
				//String balance = subObject.getString("balance");
				String id = subObject.getString("id");
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
				builder.setTitle("บันทึกข้อมูลเรียบร้อย");

				DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						switch (which){
							case DialogInterface.BUTTON_NEGATIVE:
								//No button clicked
								dialog.dismiss();
								finish();
								break;
						}
					}
				};
				builder.setMessage("")
						.setNegativeButton("ปิด", dialogClickListener).show();
				//mRemain.setText(balance);
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}
}
