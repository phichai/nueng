package com.RetailSoft.shop_op;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static java.security.AccessController.getContext;


public class Getlocation extends Activity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private GoogleApiClient googleApiClient;
    private TextView textView;
    private Button btnsave;
    private JSONObject object;
    public double lat,lon;
    public EditText idshop;
    private SavePositionAsync mSavePosition;
    private Context mContext;
    private StringBuilder str;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getlocation);
        idshop = (EditText) findViewById(R.id.idshop);
        textView = (TextView) findViewById(R.id.text_view);
        btnsave = (Button) findViewById(R.id.loca);



        // Create Google API Client instance
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        btnsave.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                mSavePosition = new SavePositionAsync();
                mSavePosition.execute();

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        // Connect to Google API Client
        googleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (googleApiClient != null && googleApiClient.isConnected()) {
            // Disconnect Google API Client if available and connected
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        // Do something when connected with Google API Client

        LocationAvailability locationAvailability = LocationServices.FusedLocationApi.getLocationAvailability(googleApiClient);
        if (locationAvailability.isLocationAvailable()) {
            // Call Location Services
            LocationRequest locationRequest = new LocationRequest()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(5000);
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        } else {
            // Do something when Location Provider not available
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        // Do something when Google API Client connection was suspended

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Do something when Google API Client connection failed

    }

    @Override
    public void onLocationChanged(Location location) {
        // Do something when got new current location
        textView.setText("Latitude : " + location.getLatitude() + "\n" +
                "Longitude : " + location.getLongitude());
        lat = location.getLatitude();
        lon = location.getLongitude();
    }



    private class SavePositionAsync extends AsyncTask<Void, Void, Void> {
       // private String test = "1";
        private String id;
        private String mylat;
        private String mylon;
        private String mResponse;
        private String ID_Shop,Lat,Lon;

        @Override
        protected Void doInBackground(Void... voids) {
            String jsonUrl ="http://192.168.1.70/database.php";
            OkHttpClient client = new OkHttpClient();
            RequestBody JSONparams = new FormEncodingBuilder()
                    .add("idshop",id)
                    .add("lat",mylat)
                    .add("lon",mylon)
                    .build();
            Request request = new Request.Builder()
                    .url(jsonUrl)
                    .post(JSONparams)
                    .build();
            try{
                Response response = client.newCall(request).execute();
                if (!response.isSuccessful()) throw new IOException("Unexpected");
                //test = "0";
                mResponse = response.body().string();
//                Toast.makeText(getApplication().getBaseContext(), ""+response.body().string(),Toast.LENGTH_LONG).show();
            }catch (Exception e){
                System.out.println("Sync .....");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            try {
                JSONObject jsonObj = new JSONObject(mResponse);
                ID_Shop = jsonObj.getString("ID_Shop");
                 Lat = jsonObj.getString("Lat");
                 Lon = jsonObj.getString("Lon");
            } catch (JSONException e) {
                e.printStackTrace();
            }


//            final String dt = str.toString();
        Log.i("TEST", ID_Shop + " : " + lat+ " : " +Lon);
//            if(test.equals("0")) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//                builder.setTitle("รายการเบิกสวัสดิการ");
//
//                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        switch (which) {
//                            case DialogInterface.BUTTON_POSITIVE:
//                                //Yes button clicked
//                                Intent aintent = new Intent(mContext, test2.class);
//                                aintent.putExtra("data", dt);
//                                startActivity(aintent);
//                                break;
//
//                            case DialogInterface.BUTTON_NEGATIVE:
//                                //No button clicked
//                                dialog.dismiss();
//                                break;
//                        }
//
//                    }
//                };
//
//            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            id=idshop.getText().toString();
            mylat=String.valueOf(lat);
            mylon=String.valueOf(lon);
        }

    }
}




