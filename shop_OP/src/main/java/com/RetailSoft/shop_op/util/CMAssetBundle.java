package com.RetailSoft.shop_op.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.RetailSoft.shop_op.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class CMAssetBundle {

  private static File directory;

/**
   * @param _dbFolderName Name of storage folder Ex. GlobalVariable.DATABASE_PATH
   * @param _orgPath Name of file in Asset Ex. dict.db
   * @param _desPath  Name of destination path Ex. GlobalVariable.DATABASE_IDIOM_NAME_HIGH_BEGINNING
   * @param _isReplace Replace file if exists? true is replace otherwise not replace
   * @param _context Context
   */
  public static void copyAssetFile(String _dbFolderName, String _orgPath, String _desPath, boolean _isReplace, Context _context) {
    if (!android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
      altDialog("Warning !!", _context.getString(R.string.sdcard_content), _context);

    } else {
      // Create the folder if it doesn't exist:
    	makedir(_dbFolderName);

      // Copy the database file if it doesn't exist:
      File file = new File(_desPath);
      if (!file.exists() || _isReplace) {
        try {
          InputStream in = _context.getAssets().open(_orgPath);
          OutputStream out = new FileOutputStream(_desPath);

          // Transfer bytes from in to out
          byte[] buf = new byte[1024];
          int len;
          while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
          }
          in.close();
          out.close();

        } catch (Exception e) {
          altDialog("Warning !!", _context.getString(R.string.sdcard_content), _context);
          e.printStackTrace();
        }
      }
    }
  }

  public static void altDialog(String sTitle, String sMessage, Context _context) {
    AlertDialog.Builder builder = new AlertDialog.Builder(_context);
    builder.setTitle(sTitle);
    builder.setMessage(sMessage).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {

      }
    });
    AlertDialog alert = builder.create();
    alert.show();
  }
  
  public static void makedir(String _folderName){
      directory = new File(_folderName);
      if (!directory.exists()) {
        directory.mkdirs();
      }
	  
  }
  
}
