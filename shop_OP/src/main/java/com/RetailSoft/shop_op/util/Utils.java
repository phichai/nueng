package com.RetailSoft.shop_op.util;

import java.io.InputStream;
import java.io.OutputStream;

public class Utils {
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
    
    public String getShopBrand(String shopCode) {
    	String pfix =  shopCode.substring(0,1);
    	if(pfix.equalsIgnoreCase("5")){
    		return "GNC";
    	}else if(pfix.equalsIgnoreCase("6")){
    		return "CPS";
    	}else if(pfix.equalsIgnoreCase("7")){
    		return "OPS";
    	}else{
    		return null;
    	}
    	
		
	}
    
    
}