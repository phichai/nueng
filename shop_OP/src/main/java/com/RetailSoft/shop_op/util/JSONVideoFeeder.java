package com.RetailSoft.shop_op.util;

import android.os.Handler;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JSONVideoFeeder {
	// XML node keys
	static final String KEY_ITEM = "ev"; // parent node
	public static final String KEY_TITLE = "title";
	public static final String KEY_FILE = "file";
	public static final String KEY_FSTATUS = "status";
	public static final String KEY_URL = "";

	private static String shopname;
	private static String shopIP;


	public static void getData(final OnJSONReceiver delegate) {
		final Handler _handler = new Handler();

		new Thread(new Runnable() {

			private DefaultHttpClient client;
			private HttpPost httpPost;
			private StringBuilder str;
			private JSONArray jObject;

			@Override
			public void run() {

				str = new StringBuilder();
				// TODO Auto-generated method stub
				final ArrayList<HashMap<String, String>> itemList = new ArrayList<HashMap<String, String>>();

				List<NameValuePair> prams = new ArrayList<NameValuePair>();
				Log.i("URL",shopIP+"//"+shopname);
				String[] parts = shopIP.replace(".", "-").split("-");
				String shopPrefix = shopname.substring(0, 1);
				String url = "http://mshop.ssup.co.th/learn/"+shopname+"/";
				if(shopname.equalsIgnoreCase("ops")){

					url="http://"+parts[0]+"."+parts[1]+"."+parts[2]+".24/mcs/learn/";
				}
				Log.i("URL",url);
				client = new DefaultHttpClient();
				httpPost = new HttpPost(url+"api_vdo_list.php");
				try {
					httpPost.setEntity(new UrlEncodedFormEntity(prams));
					HttpResponse response = client.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) { // Status OK
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							str.append(line);
						}
						Log.i("URL JSON DATA", str.toString());
						jObject = new JSONArray(str.toString());
						Log.i("URL json array",String.valueOf(jObject.length()));
						for (int i = 0; i < jObject.length(); i++) {
							HashMap<String, String> map = new HashMap<String, String>();
							JSONObject subObject = jObject.getJSONObject(i);

							map.put(KEY_TITLE, subObject.getString(KEY_TITLE));
							map.put(KEY_URL, url);
							map.put(KEY_FILE,subObject.getString(KEY_FILE));
							map.put(KEY_FSTATUS,subObject.getString(KEY_FSTATUS));

							itemList.add(map);
						}

					} else {
						Log.e("URL Log", "Failed to connect server..");
					}
				} catch (ClientProtocolException e) {
					// Log.e("Log", "Failed to connect server..");
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Log.e("Json error", "JSON Error");
					e.printStackTrace();
				}

				_handler.post(new Runnable() {

					@Override
					public void run() {
						delegate.onReceiveData(itemList);
					}
				});

			}
			/**/
		}).start();
	}

	public static void setData(String a) {
		shopname = a;
	}

	public static void setData(String brand, String deviceIP) {
		shopname = brand;
		shopIP = deviceIP;
	}
}