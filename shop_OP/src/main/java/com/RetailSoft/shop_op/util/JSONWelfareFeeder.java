package com.RetailSoft.shop_op.util;

import android.os.Handler;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class JSONWelfareFeeder {
	// XML node keys
	static final String KEY_ITEM = "ev"; // parent node
	public static final String KEY_WHERE = "usage_where";
	public static final String KEY_AMOUNT = "amount";
	public static final String KEY_CFAMOUNT = "amount_confirm";
	public static final String KEY_BECAUSE = "usage_because";
	public static final String KEY_USTATUS = "usage_status";

	public static final String KEY_DATE = "usage_hr_confirm_date";

	private static String jsdata;
	private static String shopIP;
	private static ArrayList<HashMap<String,String>> itemList;


	public static void getData(final OnJSONReceiver delegate) {
		final Handler _handler = new Handler();
		final ArrayList<HashMap<String, String>> itemList = new ArrayList<HashMap<String, String>>();
		new Thread(new Runnable() {

			private JSONArray jObject;

			@Override
			public void run() {
				try {
						Log.i("URL JSON DATA", jsdata);
						jObject = new JSONArray(jsdata);
						Log.i("URL json array",String.valueOf(jObject.length()));
						for (int i = 0; i < jObject.length(); i++) {
							HashMap<String, String> map = new HashMap<String, String>();
							JSONObject subObject = jObject.getJSONObject(i);
							map.put(KEY_WHERE, subObject.getString(KEY_WHERE));
							map.put(KEY_AMOUNT,subObject.getString(KEY_AMOUNT));
							map.put(KEY_CFAMOUNT,subObject.getString(KEY_CFAMOUNT));
							map.put(KEY_BECAUSE,subObject.getString(KEY_BECAUSE));
							map.put(KEY_DATE,subObject.getString(KEY_DATE));
							map.put(KEY_USTATUS,subObject.getString(KEY_USTATUS));
							itemList.add(map);
						}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Log.e("Json error", "JSON Error");
					e.printStackTrace();
				}

				_handler.post(new Runnable() {

					@Override
					public void run() {
						delegate.onReceiveData(itemList);
					}
				});

			}
			/**/
		}).start();
	}

	public static void setData(String a) {
		jsdata = a;
	}

}