package com.RetailSoft.shop_op;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.RetailSoft.shop_op.util.NetworkUtil;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class playerstream extends Activity {
	private String shopname="";
	private String user_id="";
	private SharedPreferences prefs;
	private String packageName = "io.vov.vitamio.demo";
	private String url_server = "http://mshop.ssup.co.th/download/";
	private Context context;
	private URL url;
	private String y_player = "y_player.apk";
	private String DeviceIP = "";
	private ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = this;
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		user_id = prefs.getString("userLogin", "");
		shopname = prefs.getString("shopName", "");
		DeviceIP = chkWIFIConnect();
        Log.i("deviece ip",DeviceIP);
		

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			shopname = shopname.substring(0, 1);
			//DeviceIP = DeviceIP.substring(0, 12);
			String[] parts = DeviceIP.split("\\.");
			String part1 = parts[0]; 
			String part2 = parts[1]; 
			String part3 = parts[2]; 
			if (shopname.equals("7")) {
				shopname = "OP";
				DeviceIP = part1+"."+part2+"."+part3+".24/download/";
				//DeviceIP = "192.168.3.247/download/";
			} else if (shopname.equals("6")) {
				shopname = "CPS";
				DeviceIP = part1+"."+part2+"."+part3+".14/download/";
			} else if (shopname.equals("5")) {
				shopname = "GNC";
				DeviceIP = part1+"."+part2+"."+part3+".4/download/";
			}
			// Toast.makeText(this, "shop="+shopname.toString(),
			// Toast.LENGTH_SHORT).show();
			Log.i("IP_Address",DeviceIP);
		}
		
		if (android.os.Build.VERSION.SDK_INT > 10) {
		     Intent intent = new Intent(playerstream.this,PlayVideo.class);
		     intent.putExtra("user_id", user_id);
		     intent.putExtra("shopname", shopname);
		     startActivity(intent);
             finish();
		 }else{

		boolean Installed = isAppInstalled(packageName);
		final AlertDialog.Builder ad = new AlertDialog.Builder(this);
		if (Installed) {
			Intent LaunchIntent = getPackageManager()
					.getLaunchIntentForPackage("io.vov.vitamio.demo");
			LaunchIntent.putExtra("user_id", user_id);
			LaunchIntent.putExtra("shopname", shopname);
			startActivity(LaunchIntent);
		} else {
//			ad.setTitle("กรุณาติดตั้งแอปพลิเคชัน Y-Player");
//			ad.setPositiveButton("Close", null);
//			ad.setMessage("Please Download Application");
//			ad.show();
			
				
					Intent intent = null;
					try {
						
						try {
							url = new URL("http://" + DeviceIP + y_player);
						} catch (MalformedURLException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
						URLConnection conn = null;
						try {
							conn = url.openConnection();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						HttpURLConnection httpconn = (HttpURLConnection) conn;
						int responseCode = 0;
						try {
							responseCode = httpconn.getResponseCode();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (responseCode == HttpURLConnection.HTTP_OK) {
							// Log.i("coming","200 pass");
							// httpconn.disconnect();
							// intent = new Intent(Intent.ACTION_VIEW,
							// Uri.parse("http://"+DeviceIP+y_player));
							// startActivity(intent);
							// execute this when the downloader must be fired
							final DownloadTask downloadTask = new DownloadTask(
									context);
							downloadTask.execute("http://" + DeviceIP
									+ y_player);
						} else {
							// Log.i("coming","200 fail");
							// intent = new Intent(Intent.ACTION_VIEW,
							// Uri.parse(url_server+y_player));
							// startActivity(intent);
							final DownloadTask downloadTask = new DownloadTask(
									context);
							downloadTask.execute(url_server + y_player);
						}
					} catch (ActivityNotFoundException e) {
						
						// intent = new Intent(Intent.ACTION_VIEW,
						// Uri.parse(url_server+y_player));
						// startActivity(intent);
						final DownloadTask downloadTask = new DownloadTask(
								context);
						downloadTask.execute(url_server + y_player);
					}
	

			}
		}
	}

	private String chkWIFIConnect() {
		// TODO Auto-generated method stub
		String tmp = NetworkUtil.getIPAddress(true);
		return tmp;
	}

	protected boolean isAppInstalled(String packageName) {
		Intent mIntent = getPackageManager().getLaunchIntentForPackage(
				packageName);
		if (mIntent != null) {
			return true;
		} else {
			return false;
		}
	}

	private class DownloadTask extends AsyncTask<String, Integer, String> {

		private Context context;
		private PowerManager.WakeLock mWakeLock;

		public DownloadTask(Context context) {
			this.context = context;
		}

		@Override
		protected String doInBackground(String... sUrl) {
			InputStream input = null;
			OutputStream output = null;
			HttpURLConnection connection = null;
			try {
				URL url = new URL(sUrl[0]);
				connection = (HttpURLConnection) url.openConnection();
				connection.connect();

				// expect HTTP 200 OK, so we don't mistakenly save error report
				// instead of the file
				if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
					return "Server returned HTTP "
							+ connection.getResponseCode() + " "
							+ connection.getResponseMessage();
				}

				// this will be useful to display download percentage
				// might be -1: server did not report the length
				int fileLength = connection.getContentLength();

				// download the file
				input = connection.getInputStream();
				output = new FileOutputStream("/sdcard/y_player.apk");

				byte data[] = new byte[4096];
				long total = 0;
				int count;
				while ((count = input.read(data)) != -1) {
					// allow canceling with back button
					if (isCancelled()) {
						input.close();
						return null;
					}
					total += count;
					// publishing the progress....
					if (fileLength > 0) // only if total length is known
						publishProgress((int) (total * 100 / fileLength));
					output.write(data, 0, count);
				}
			} catch (Exception e) {
				return e.toString();
			} finally {
				try {
					if (output != null)
						output.close();
					if (input != null)
						input.close();
				} catch (IOException ignored) {
				}

				if (connection != null)
					connection.disconnect();
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// take CPU lock to prevent CPU from going off if the user
			// presses the power button during download
			PowerManager pm = (PowerManager) context
					.getSystemService(Context.POWER_SERVICE);
			mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
					getClass().getName());
			mWakeLock.acquire();
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage("กำลังดาวโหลดแอปพลิเคชั่น");
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setCancelable(true);
			mProgressDialog.show();
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			super.onProgressUpdate(progress);
			// if we get here, length is known, now set indeterminate to false
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.setMax(100);
			mProgressDialog.setProgress(progress[0]);
		}

		@Override
		protected void onPostExecute(String result) {
			mWakeLock.release();
			mProgressDialog.dismiss();
			if (result != null)
				Toast.makeText(context, "Download error: " + result,
						Toast.LENGTH_LONG).show();
			else {
				// Toast.makeText(context,"File downloaded",
				// Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setAction(android.content.Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.parse("file:///sdcard/y_player.apk"),
						"application/vnd.android.package-archive");

				startActivity(intent);

				finish();
			}

		}

	}// DownloadTask

}
