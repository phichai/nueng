package com.RetailSoft.shop_op;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CallLog;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PhoneStateReceiver extends BroadcastReceiver {

	private Intent mIntent;
	private long start_time;
	private long start_timetmp;
	private long end_time;
	private Context mContext;
	private StringBuffer rdata;
	private String memberNo;
	private String userID;
	private SharedPreferences pref;
	private DefaultHttpClient client;
	private HttpPost httpPost;
	private String callTransID;
	private StringBuilder str;
	private CallUpdate McallUpdateTask;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub

		mContext = context;
		Bundle extras = intent.getExtras();

		pref = PreferenceManager.getDefaultSharedPreferences(mContext);
		callTransID = pref.getString("calltransID", "");

		if (extras != null) {
			final String state = extras.getString(TelephonyManager.EXTRA_STATE);
			Log.i("MY_PHONE_STATE", state);
			
			McallUpdateTask = new CallUpdate();
			McallUpdateTask.setQtr(state);
			McallUpdateTask.execute();
			
			

			if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
				String phoneNumber = extras
						.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
				Log.i("MY_PHONE_STATE", phoneNumber);

			}
		}

		/*
		 * TelephonyManager telephony =
		 * (TelephonyManager)context.getSystemService
		 * (Context.TELEPHONY_SERVICE); telephony.listen(new
		 * PhoneStateListener(){
		 * 
		 * @Override public void onCallStateChanged(int state, String
		 * incomingNumber) { super.onCallStateChanged(state, incomingNumber);
		 * Log.i("State", String.valueOf(state));
		 * Log.i("calling No",incomingNumber); switch(state){ case
		 * TelephonyManager.CALL_STATE_RINGING : Log.i("State", "Incoming");
		 * break; case TelephonyManager.CALL_STATE_OFFHOOK : Log.i("State",
		 * "Calling"); Log.i("calling No",incomingNumber); break; case
		 * TelephonyManager.CALL_STATE_IDLE: Log.i("State", "Idle"); break; }
		 * 
		 * 
		 * } },PhoneStateListener.LISTEN_CALL_STATE);
		 * 
		 * /*
		 */
		/*
		 * String action = intent.getAction();
		 * if(action.equalsIgnoreCase("android.intent.action.PHONE_STATE")){ if
		 * (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
		 * TelephonyManager.EXTRA_STATE_RINGING)) {
		 * Log.i("incoming No.",TelephonyManager.EXTRA_INCOMING_NUMBER);
		 * start_time=System.currentTimeMillis();
		 * 
		 * } if (mIntent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
		 * TelephonyManager.EXTRA_STATE_IDLE)) {
		 * end_time=System.currentTimeMillis(); //Total time talked = long
		 * total_time = end_time-start_time; //Store total_time somewhere or
		 * pass it to an Activity using intent
		 * Log.i("call duration",String.valueOf(total_time));
		 * 
		 * }
		 * 
		 * } /*
		 */
	}
	
	class CallUpdate extends AsyncTask<String, Integer, Void> {
		String callST;
		
		protected void setQtr(String callstate) {
			callST = callstate;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			Log.i("CallDAta",callST);
		}
		
		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub
			List<NameValuePair> prams = new ArrayList<NameValuePair>();
			prams.add(new BasicNameValuePair("callState", callST));
			prams.add(new BasicNameValuePair("calltransID", callTransID));
			String url = "http://mshop.ssup.co.th/shop_op/update_calling.php";
			client = new DefaultHttpClient();
			httpPost = new HttpPost(url);
			str = new StringBuilder();
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(prams));
				HttpResponse response = client.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) { // Status OK
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						str.append(line);
					}
					
				} else {
					Log.e("Log", "Failed to connect server..");
				}
				
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				Log.i("RESULT",str.toString());
			
		}
		
	}

	private StringBuffer getCallDetails(String[] extraIncomingNumber) {

		StringBuffer sb = new StringBuffer();
		// mContext.getContentResolver().query(uri, projection, selection,
		// selectionArgs, sortOrder)
		Cursor managedCursor = mContext.getContentResolver().query(
				CallLog.Calls.CONTENT_URI, null, CallLog.Calls.NUMBER + "=?",
				extraIncomingNumber, null);
		int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
		int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
		int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
		int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
		sb.append("Call Details :");
		while (managedCursor.moveToNext()) {
			String phNumber = managedCursor.getString(number);
			String callType = managedCursor.getString(type);
			String callDate = managedCursor.getString(date);
			Date callDayTime = new Date(Long.valueOf(callDate));
			String callDuration = managedCursor.getString(duration);
			String dir = null;
			int dircode = Integer.parseInt(callType);
			switch (dircode) {
			case CallLog.Calls.OUTGOING_TYPE:
				dir = "OUTGOING";
				break;

			case CallLog.Calls.INCOMING_TYPE:
				dir = "INCOMING";
				break;

			case CallLog.Calls.MISSED_TYPE:
				dir = "MISSED";
				break;
			}
			sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
					+ dir + " \nCall Date:--- " + callDayTime
					+ " \nCall duration in sec :--- " + callDuration);
			sb.append("\n----------------------------------");
		}
		managedCursor.close();
		// call.setText(sb);
		return sb;
	}

}
